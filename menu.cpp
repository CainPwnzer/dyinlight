#include <Windows.h>
#include <iostream>
#include <intrin.h>
#include <thread>
#include <chrono> 
#include <ctime> 
#include "stdio.h"
#include <iomanip>
#include "hacks.h"
#include "memory.h"
#include <sstream>
#include <string>


//imgui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_impl_dx11.h"
#include <TlHelp32.h>
#include <list>
#include <set>

#include "Print.hpp"
//offsets
char* attachto = "gamedll_x64_rwdi.dll";
int indexoffset = 0x1C901E0;
DWORD64 moduleBase = (DWORD64)GetModuleHandle(TEXT(attachto));
DWORD ApplicationPID = GetProcessId(GetCurrentProcess());
uintptr_t modulesize = GetModuleSize(ApplicationPID, attachto);

///menu values
bool greetings = true;
bool showdebug = false;
bool bHealth = false;
bool bAmmo = false;
bool bRecoil = false;
float fJump = 1;
bool ammoon = false;
bool healthon = false;
bool ShowMenu = false;
bool firstTime = true;
bool bkill = false;
bool needaobs = true;

//wallhack
bool ModelrecFinder = true;
int Wallhack = 1;
bool DeleteTexture = true;

//logger, misc
bool logger = false;
int countnum = -1;
int countStride = -1;
int countIndexCount = -1;
int countpscdescByteWidth = -1;
int countindescByteWidth = -1;
int countvedescByteWidth = -1;

DWORD_PTR testaddress;
DWORD_PTR dwAddress;

using namespace std;

vector<string> Vaobresult;

void dothehacks()
{
	//debugstuff();
	
	aobgrab();
	
	//shitty();

	if (ShowMenu)
	{
		ImGuiStyle* style = &ImGui::GetStyle();
		style->WindowPadding = ImVec2(15, 15);
		style->WindowRounding = 5.0f;
		style->FramePadding = ImVec2(5, 5);
		style->FrameRounding = 4.0f;
		style->ItemSpacing = ImVec2(12, 8);
		style->ItemInnerSpacing = ImVec2(8, 6);
		style->IndentSpacing = 25.0f;
		style->ScrollbarSize = 15.0f;
		style->ScrollbarRounding = 9.0f;
		style->GrabMinSize = 5.0f;
		style->GrabRounding = 3.0f;
		style->Colors[ImGuiCol_Text] = ImVec4(0.80f, 0.80f, 0.83f, 1.00f);
		style->Colors[ImGuiCol_TextDisabled] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_PopupBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_Border] = ImVec4(0.80f, 0.80f, 0.83f, 0.88f);
		style->Colors[ImGuiCol_BorderShadow] = ImVec4(0.92f, 0.91f, 0.88f, 0.00f);
		style->Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_TitleBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		//style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.98f, 0.95f, 0.75f);
		style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_CheckMark] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_Header] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_Column] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ColumnHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_ColumnActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ResizeGrip] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		style->Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_PlotLines] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
		style->Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
		style->Colors[ImGuiCol_PlotHistogram] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
		style->Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
		style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.25f, 1.00f, 0.00f, 0.43f);
		style->Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(1.00f, 0.98f, 0.95f, 0.73f);
		ImGui::SetNextWindowPos(ImVec2(0, 0));
		ImGui::SetNextWindowBgAlpha(0.50f);
		ImGui::Begin("Dying Light Mod | GamePwnzer & Cain532");
		//ImGui::Checkbox("Wallhack Texture", &Wallhack);

		if (ImGui::CollapsingHeader("Player Modification"))
		{
			ImGui::Checkbox("Godmode", &bHealth); //turns on but not off.
			ImGui::Checkbox("Unlimited Ammo ", &bAmmo); //working
			ImGui::Checkbox("Instant Kill ", &bkill); //working
		}
		if (ImGui::CollapsingHeader("WallHack")) //works.
		{
			const char* Wallhack_Options[] = { "Off", "DepthStencil", "DepthBias" };
			ImGui::Text("Wallhack Texture");
			ImGui::SameLine();
			ImGui::Combo("##Wallhack", (int*)&Wallhack, Wallhack_Options, IM_ARRAYSIZE(Wallhack_Options));

			ImGui::Checkbox("Delete Texture ", &DeleteTexture); //the point is to highlight textures to see which we are logging
			ImGui::Checkbox("Modelrec Finder", &ModelrecFinder);

			if (ModelrecFinder)
			{
				ImGui::SliderInt("find Stride", &countStride, -1, 148);

				if (countIndexCount >= -1 && countIndexCount <= 147)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, -1, 148);
				}
				else if (countIndexCount >= 148 && countIndexCount <= 295)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, 149, 296);
				}
				else if (countIndexCount >= 296 && countIndexCount <= 443)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, 297, 444);
				}
				else if (countIndexCount >= 444 && countIndexCount <= 591)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, 445, 592);
				}
				else if (countIndexCount >= 592 && countIndexCount <= 739)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, 593, 740);
				}
				else if (countIndexCount >= 740 && countIndexCount <= 887)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, 741, 888);
				}
				else if (countIndexCount >= 888 && countIndexCount <= 1035)
				{
					ImGui::SliderInt("find IndexCount", &countIndexCount, 889, 1036);
					if (countIndexCount == 1036)
						countIndexCount = -1;
				}

				ImGui::SliderInt("find pscdesc.ByteWidth", &countpscdescByteWidth, -1, 148);
				ImGui::SliderInt("find indesc.ByteWidth", &countindescByteWidth, -1, 148);
				ImGui::SliderInt("find vedesc.ByteWidth", &countvedescByteWidth, -1, 148);

				ImGui::Text("ImGui Menu Navigation: ");
				ImGui::Text("Use TAB & Arrows or Mouse to navigate, Space to select options");
				ImGui::Text("Press F9 to log draw functions");
				ImGui::Text("Press END to log deleted textures");
				ImGui::Spacing();
				ImGui::Text("Hotkeys:");
				ImGui::Text("ALT + F1 toggles Wallhack");
				ImGui::Text("ALT + F2 toggles DeleteTexture");
				ImGui::Text("ALT + F3 toggles ModelrecFinder");
				ImGui::Text("Use Page Up/Down to find Stride");
				ImGui::Text("Use 7/8 to find IndexCount");
			}
		}
		ImGui::End();
	}

	if (!bAmmo)
	{
		mem::Patch((BYTE*)(strtohex(4) + 0x07), (BYTE*)"\xFF\xCA", 2);
		mem::Patch((BYTE*)(strtohex(5) + 0x06), (BYTE*)"\x45\x89\x44\xC2\x04", 5);
		bool ammoon = false;
	}
	if (bAmmo)
	{
		mem::Patch((BYTE*)(strtohex(4) + 0x07), (BYTE*)"\x90\x90", 2);
		mem::Patch((BYTE*)(strtohex(5) + 0x06), (BYTE*)"\x90\x90\x90\x90\x90", 5);
		bool ammoon = true;
	}
	if (bHealth)
	{
		mem::Patch((BYTE*)(strtohex(1)), (BYTE*)"\x90\x90\x90", 3);
		mem::Patch((BYTE*)(strtohex(2)), (BYTE*)"\x90\x90\x90", 3);
		bool healthon = true;
	}
	if (!bHealth)
	{
		mem::Patch((BYTE*)(strtohex(1)), (BYTE*)"\x0F\x2F\xF7", 3);
		mem::Patch((BYTE*)(strtohex(2)), (BYTE*)"\x0F\x2F\xF0", 3);
		bool healthon = false;
	}
	if (bkill)
	{
		mem::Patch((BYTE*)(strtohex(0)), (BYTE*)"\xF3\x0F\x5C\xC9\xF3\x41\x0F\x11\x4F\x78", 10);
	}
	if (!bkill)
	{
		mem::Patch((BYTE*)(strtohex(0)), (BYTE*)"\xF3\x0F\x5C\xCE\xF3\x41\x0F\x11\x4F\x7C", 8);
	}

	ImGui::EndFrame();
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

DWORD GetModuleSize(DWORD processID, char* module)
{

	HANDLE hSnap;
	MODULEENTRY32 xModule;
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, processID);
	xModule.dwSize = sizeof(MODULEENTRY32);
	if (Module32First(hSnap, &xModule)) {
		while (Module32Next(hSnap, &xModule)) {
			if (!strncmp((char*)xModule.szModule, module, 8)) {
				CloseHandle(hSnap);
				return (DWORD)xModule.modBaseSize;
			}
		}
	}
	CloseHandle(hSnap);
	return 0;
}

DWORD_PTR dectohex(DWORD_PTR decin)
{
	DWORD_PTR hexout;
	std::cout << hex << decin;
	std::cin >> hexout;
	return hexout;
}

void debugstuff()
{

	if (showdebug) {
		//DWORD ApplicationPID = GetProcessId(GetCurrentProcess());
		//DWORD64 User32Size = GetModuleSize(ApplicationPID, attachto);
		//dwAddress = (DWORD64)GetModuleHandle(attachto);
		//DWORD64 testaddress = FindPattern(dwAddress, User32Size, (PBYTE)"\xF3\x0F\x5C\xCE\xF3\x41\x0F\x11", "xxxxxxxx");
		auto timenow = chrono::system_clock::to_time_t(chrono::system_clock::now());
		//uintptr_t pPlayer = moduleBase + indexoffset;
		//uintptr_t oAmmo = mem::Findptr(moduleBase + indexoffset, { 0x918, 0x48, 0x40, 0x04 });
		//uintptr_t oHealth = mem::Findptr(moduleBase + indexoffset, { 0x122c });
		//int myammo = *(int*)oAmmo;
		//int myhealth = *(int*)oHealth;
		//float zzz;
		//zzz = *((float*)&myhealth);
		
		AllocConsole();
		FILE* f;
		freopen_s(&f, "CONOUT$", "w", stdout);
		SetConsoleTitle("Debug- Dying Light");
		std::cout << "---------------------------------------------------------------------" << endl;
		
		//if (firstaob)
		//{
		//DWORD64 testaddress = FindPattern(dwAddress, User32Size, (PBYTE)"\xF3\x0F\x5C\xCE\xF3\x41\x0F\x11", "xxxxxxxx");
		//	bool firstaob = false;
		//}
		
		//std::cout << "Current time: " << ctime(&timenow) << endl;
		//std::cout << "Dll Name : " << attachto << endl;
		//std::cout << "Dll Loaded @ " << moduleBase << endl;
		//std::cout << "player base pointer: " << hex << pPlayer << endl;
		//std::cout << "Rifle Ammo offset: " << hex << oAmmo << endl;
		//std::cout << "Rifle Ammo Value: " << dec << myammo << endl;
		//std::cout << "Health Offset: " << hex << oHealth << endl;
		//std::cout << "Health Value: " << std::setprecision(3) << zzz << endl;
		//std::cout << "\n";

		//DWORD testaddress = FindPattern(moduleBase, User32Size, "\xF3\x0F\x5C\xCE\xF3\x41\x0F\x11", "xxxxxxxx");
		/*std::cout << "Process ID (HEX): " << ApplicationPID << endl;
		std::cout << "Process ID (DEC): " << std::dec << ApplicationPID << endl;*/
		//std::cout << "DLL STARTING OFFSET: " << (uintptr_t)GetModuleHandle(attachto) << endl;
		//std::cout << "DLL ENDING OFFSET: " << dwAddress + User32Size << endl;
		//std::cout << "DLL SIZE (HEX): " << User32Size << endl;
		//std::cout << "AOB RANGE: " << dwAddress << "-" << dwAddress + User32Size << endl;
		//std::cout << "AOB RESULT: " << testaddress << endl;
		//std::cout << "\n" << endl;
		//DWORD64 bla;
		//printf("!!APPID: %llX\n", ApplicationPID);
		//printf("!!DLL BASE START: 0x%llX\n", dwAddress);
		//printf("!!DLL BASE END: 0x%llX\n", dwAddress + User32Size);
		//printf("!!DLL SIZE: %llX\n", User32Size);
		//printf("!!AOB SCAN RESULT: 0x%llX\n", testaddress);
		//char szMyStringz[100] = { 0 };
		//sprintf(szMyStringz, "AOB SCAN COMPLETE \n OFFSET FOUND!: \n 0x%llX\n", testaddress);
		//MessageBox(NULL, szMyStringz,"BINGO1!", MB_OK);
		//char szMyString[10] = { 0 };
		//sprintf(szMyString, "%08x", dwAddress);
		//MessageBox(NULL, szMyString, TEXT("dwAddress"), MB_OK);

		//char szMyStringz[10] = { 0 };
		//sprintf(szMyStringz, "%08x", User32Size);
		//MessageBox(NULL, szMyStringz, TEXT("User32Size"), MB_OK);

		//std::cout << "AOB SEARCH FOR: \xF3\x0F\x5C\xCE\xF3\x41\x0F\x11" << endl;
		std::cout << "---------------------------------------------------------------------" << endl;
		Sleep(2000);
	}
}

void openconsole() {
	AllocConsole();
	FILE* f;
	freopen_s(&f, "CONOUT$", "w", stdout);
	SetConsoleTitle("Debug- Dying Light");
};

uintptr_t strtohex(int vnumber) 
{
	string hexStr = Vaobresult[vnumber];
	uintptr_t x;
	stringstream ss;
	ss << std::hex << hexStr;
	ss >> x;
	//cout << "lets see HEX uintptr_t:  " << vnumber  << ": "<< x << endl;
	return x;
}


//int dectohex(int decimal) {
//	std::stringstream stream;
//	stream << std::hex << decimal;
//	std::string nowhex(stream.str());
//	return nowhex;
//}



//AOBType aobs[] = {
//	AOBType { "instantkill", "\xF3\x0F\x5C\xCE\xF3\x41\x0F\x11", "xxxxxxxx"},
//	AOBType { "infhealth", "\x0F\x2F\xF7\x73\x05\x0F\x28\xC7\xEB\x08", "xxxxxxxxxx" },
//	AOBType { "infhealth2", "\0F\x2F\xF0\x77\x03\x0F\x28\xC6\x44", "xxxxxxxxxx" },
//	AOBType { "indestructable" , "\x0F\x2F\xF0\x76\x11\x48\x8B\x47\x18", "xxxxxxxxx"},
//	AOBType { "infammo1", "\x8B\x50\x40\x48\x8D\x48\x40\xFF\xCA\xE8", "xxxxxxxxxx"},
//	AOBType { "infammo2", "\x48\x63\xC3\x48\x03\xC0\x45\x89\x44\xC2\x04", "xxxxxxxxxxx"},
//	AOBType { "infstamina1", "\xF3\x0F\x10\x4B\x10\xF3\x41\x0F\x5C\xC8", "xxxxxxxxxx" },
//	AOBType { "infstamina2", "\xF3\x0F\x5C\xC7\xF3\x0F\x11\x43\x10", "xxxxxxxxx" }
//};

AOBname AOBArray[]{
	AOBname { "instantkill", "\xF3\x0F\x5C\xCE\xF3\x41\x0F\x11", "xxxxxxxx"},
	AOBname { "infhealth", "\x0F\x2F\xF7\x73\x05\x0F\x28\xC7\xEB\x08", "xxxxxxxxxx" },
	AOBname { "infhealth2", "\x0F\x2F\xF0\x77\x03\x0F\x28\xC6\x44", "xxxxxxxxx" },
	AOBname { "indestructable" , "\x0F\x2F\xF0\x76\x11\x48\x8B\x47\x18", "xxxxxxxxx"},
	AOBname { "infammo1", "\x8B\x50\x40\x48\x8D\x48\x40\xFF\xCA\xE8", "xxxxxxxxxx"},
	AOBname { "infammo2", "\x48\x63\xC3\x48\x03\xC0\x45\x89\x44\xC2\x04", "xxxxxxxxxxx"},
	AOBname { "infstamina1", "\xF3\x0F\x10\x4B\x10\xF3\x41\x0F\x5C\xC8", "xxxxxxxxxx" },
	AOBname { "infstamina2", "\xF3\x0F\x5C\xC7\xF3\x0F\x11\x43\x10", "xxxxxxxxx" } 
};

void aobgrab()
{
	//openconsole();
	if (needaobs) {
		for (int i = 0; i < sizeof(AOBArray) / sizeof(AOBArray[0]); i++)
		{
			uintptr_t result = FindPattern(moduleBase, modulesize, (PBYTE)AOBArray[i].pattern, AOBArray[i].mask);
			
			std::stringstream ss;
			ss << std::hex << result;
			std::string value(ss.str());

			//Vaobresult.insert(Vaobresult.begin() + i, result);

			Vaobresult.insert(Vaobresult.begin() + i, value);
		}
		//cout << "Size of Vaobresult  : " << Vaobresult.size() << "\n";
		
		//for (auto iz : Vaobresult)
		//{
		//	cout << "Stored Vaobresult values :" << iz << "\n";
		//}
		
		//for (int p = 0; p < Vaobresult.size(); ++p) //converted from string value to uintptr_t.
		//{
		//	std::cout << Vaobresult[i] << ' ' << "\n";
		//	cout << "Value of number[" << p << "]: " << strtohex(p) << endl;
		//}
		//for (int p = 0; p < sizeof(Vaobresult) / sizeof(Vaobresult[0]); p++)
		//{
		//	cout << "Value of number p: " << strtohex(p) << endl;
		//}

		//DWORD64& vinstantkill = ;
		needaobs = false;
		//Sleep(100);
	}
}