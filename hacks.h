///menu values
#include <string>
extern bool greetings;
extern bool showdebug;
extern bool bHealth;
extern bool bAmmo;
extern bool bRecoil;
extern float fJump;
extern bool ammoon;
extern bool bkill;
extern bool healthon;
extern bool ShowMenu;
extern bool firstTime;
extern char* attachto;
extern bool needaobs;

//wallhack
extern bool ModelrecFinder;
extern int Wallhack;
extern bool DeleteTexture;

//logger, misc
extern bool logger;
extern int countnum;
extern int countStride;
extern int countIndexCount;
extern int countpscdescByteWidth;
extern int countindescByteWidth;
extern int countvedescByteWidth;


//offsets
extern uintptr_t moduleBase;
extern int indexoffset;
extern DWORD64 testaddress;

//functions
extern DWORD GetModuleSize(DWORD processID, char* module);
extern DWORD64 FindPattern(DWORD64 dwStart, DWORD64 dwLen, BYTE* pszPatt, char pszMask[]);
extern void debugstuff();
extern void dothehacks();
extern void aobgrab();


//aob stuff


uintptr_t strtohex(int vnumber);
struct AOBType
{
	std::string hackname;
	const char* pattern;
	char* mask;
};

struct AOBname {

	std::string aobname;
	char* pattern;
	char* mask;

};