#include <Windows.h>
#include <iostream>
#include <Windows.h>
#include <tlhelp32.h>
#include <Psapi.h>
#include "hacks.h"

using namespace std;
DWORD64 FindPattern(DWORD64 dwStart, DWORD64 dwLen, BYTE* pszPatt, char pszMask[]) {
	unsigned int i = NULL;
	auto iLen = strlen(pszMask) - 1;
	for (DWORD64 dwRet = dwStart; dwRet < dwStart + dwLen; dwRet++) {
		if (*(BYTE*)dwRet == pszPatt[i] || pszMask[i] == '?')
		{
			if (pszMask[i + 1] == '\0') 
				return(dwRet - iLen); i++;

		}
		else i = NULL;
	}
	return NULL;
}